\documentclass[10pt,aps,prb,twocolumn,amsmath,amssymb,superscriptaddress]{revtex4-1}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{color}
\usepackage{multirow}
\usepackage[caption = false]{subfig}

\def\beq{\begin{eqnarray}}
\def\eeq{\end{eqnarray}}
\def\beqq{\begin{eqnarray*} \color{blue} }
\def\eeqq{\end{eqnarray*}}
\def\OpOne{\hat{\mathbf{1}}}
\def\rvec{{\mathbf r}}
\def\V{\mathcal{V}}
\def\C{\mathcal{C}}
\def\NMC{{N_{\mathrm{MC}}}}
\def\NpMC{{N'_{\mathrm{MC}}}}
\def\NppMC{{N''_{\mathrm{MC}}}}
\def\NMCdiff{{N_{\mathrm{MC}}^{\mathrm{diff}}}}
\def\Nd{{N_d}}
\def\Nddiff{{N_{d}^{\mathrm{diff}}}}
\newcommand{\bra}[1]{\left\langle #1 \right|}
\newcommand{\ket}[1]{\left|  #1  \right\rangle}
\def\eh{{\rm E}_h}
\def\meh{{\rm mE}_h}

\begin{document}

%\title{Chromium dimer potential energy surface in the full configuration interaction limit}
%\title{Potential energy surface of the chromium dimer in the full configuration interaction limit}
\title{An efficient quantum many-body algorithm with application to the strongly-correlated chromium dimer}
%\title{A Heat-bath Configuration Interaction study of the Chromium Dimer}

\author{Adam A. Holmes}
\affiliation{Department of Chemistry and Biochemistry, University of Colorado Boulder, Boulder, CO 80302, USA}
\affiliation{Laboratory of Atomic and Solid State Physics, Cornell University, Ithaca, NY 14853, USA}

\author{Matthew Otten}
\affiliation{Laboratory of Atomic and Solid State Physics, Cornell University, Ithaca, NY 14853, USA}
\affiliation{Lawrence Livermore National Laboratory, Livermore, California 94550, USA}

\author{Qiming Sun}
\affiliation{Division of Chemistry and Chemical Engineering, California Institute of Technology, Pasadena, California 91125, USA}

\author{Sandeep Sharma}
\affiliation{Department of Chemistry and Biochemistry, University of Colorado Boulder, Boulder, CO 80302, USA}

\author{C. J. Umrigar}
\affiliation{Laboratory of Atomic and Solid State Physics, Cornell University, Ithaca, NY 14853, USA}


\begin{abstract}
We present highly accurate first-principles calculations of the ground-state potential energy surface of the strongly-correlated chromium dimer using the recently-developed semistochastic Heat-bath Configuration Interaction (HCI) method, which enables efficient extrapolation to the exact ground-state energy of a many-body Hamiltonian within a given single-particle basis set.
We compute the potential energy surface in two ways: correlating only the 12 valence electrons in up to 190 orbitals, and including semi-core excitations by correlating 28 electrons in 76 orbitals.
%We perform both frozen Ar core calculations in basis sets as large as cc-pVQZ-DK (12e, 190o), and frozen Ne core calculations in the cc-pVDZ-DK basis (28e, 76o).
The results presented in this paper demonstrate that (1) HCI is capable of accurately and efficiently solving the many-body Schrodinger equation even for strongly-correlated systems such as Cr$_2$, and (2) semi-core excitations are necessary for a qualitatively correct description of the binding curve of Cr$_2$.
%These results demonstrate that a qualitatively correct description of the binding curve can be obtained if semi-core excitations are included.
\end{abstract}

\maketitle

\section{Introduction}
The accurate first-principles calculations of strongly-correlated molecules and materials is of fundamental importance in condensed matter physics and quantum chemistry.
Mean-field theories such as density functional theory have shown great success for many systems in which the independent particle picture is a reasonable zeroth-order approximation.
However, more accurate wavefunction-based methods must be used to treat strongly-correlated systems.

In solving the many-body Schrodinger equation for such systems, it is often convenient to work in a second-quantized basis of linear combinations of products of a finite set of one-particle states or orbitals, such as Slater determinants.
Even with this simplification, the number of
such Slater determinants
%electron configurations, or assignments of electrons to orbitals,
increases combinatorially with system size. Thus, for all but the smallest toy systems, the size of the Hilbert space is much too large to store.

Some of us recently developed the Heat-bath Configuration Interaction (HCI) algorithm~\cite{Holmes2016,holmes2017excited,smith2017cheap}, which, combined with semistochastic pertubation theory~\cite{sharma2017semistochastic}, is a highly efficient method for solving the second-quantized many-body Schrodinger equation.
HCI is a simple, efficient method that has some advantages over other state-of-the-art methods.
In contrast to the density matrix renormalization group (DMRG), which correlates orbitals in a linear chain, HCI naturally works equally well in any number of spatial dimensions.
Unlike full configuration interaction quantum Monte Carlo (FCIQMC),~\cite{Booth2009,Cleland2010,PetHolChaNigUmr-PRL-12,Booth2013} the bulk of the HCI calculation is deterministic, and the stochasticity in HCI is only used to stochastically sum a series, so there is no fermion sign problem or autocorrelation time.
The extremely long autocorrelation time of FCIQMC likely makes accurate calculations of molecules as complex as Cr$_2$ very computationally demanding.

In this work, we use HCI to calculate the ground-state potential energy surface of Cr$_2$, including scalar relativistic effects with the Douglas-Kroll-Hess Hamiltonian~\cite{douglas1974quantum,hess1986relativistic,jansen1989revision} and extrapolating the resulting ground-state energies to the limit of the exact answer within the basis set (the Full CI limit).
We demonstrate that, even for large basis sets, correlating the 12 valence electrons alone is not enough to provide a qualitatively correct curve, even for basis sets as large as cc-pVQZ-DK (190 valence orbitals).
However, when semi-core excitations are included and 28 electrons are correlated, even the smaller
cc-pVDZ-DK basis set (76 valence orbitals) is enough for a qualitatively correct potential energy surface from first principles.

The remainder of this paper is organized as follows.
In Section~\ref{hci}, we review the HCI algorithm, a general quantum many-body algorithm that can be used with any second-quantized Hamiltonian.
In Section~\ref{related}, we review the state of the art in \emph{ab initio} calculations on the chromium dimer.
In Section~\ref{results}, we present our HCI results on the chromium dimer, modeled with both 12 and 28 correlated electrons. Finally, we draw some conclusions and present some concluding remarks in Section~\ref{conclusion}.

\section{Semistochastic Heat-bath Configuration Interaction}
\label{hci}

Consider the ubiqitous problem of finding the ground state of a second-quantized many-body Hamiltonian
\beq
\hat{H}&=&h^{(0)}+\sum_p h^{(1)}_p\left(a_p^\dagger+a_p\right)\\
&&+\sum_{pq}\left[ h^{(2a)}_{pq}a_{q}^\dagger a_p + h^{(2b)}_{pq}\left(a_q^\dagger a_p^\dagger + a_q a_p\right)\right]+ \ldots,\nonumber
\eeq
where $a_{p}^{\dagger} (a_{p})$ is a single-particle creation (annihilation) operator corresponding to the $p^{\rm th}$ single-particle basis state.
The Hamiltonian may contain terms with any numbers of creation and annihilation operators, and need not even conserve particle number.

The time-independent Schrodinger equation is a simple eigenvalue equation, $\hat{H}\psi=E\psi$, where $\psi$ is a linear combination of basis states which are composed of products of single-particle states, such as Slater determinants. However, even in this basis, the Schrodinger equation is difficult to solve, as the Hilbert space itself scales combinatorially with system size and is therefore in general too large to store at one time. 


A useful class of algorithms for such problems is Selected Configuration Interaction with Perturbation Theory (SCI+PT), which proceeds in two steps:
\begin{enumerate}
\item  a variational wavefunction is found by computing the ground state within an ``important'' subset $\mathcal{V}$ of the Hilbert space, then
\item  perturbation theory is performed to improve the variational energy.
\end{enumerate}

To find the important subset of the Hilbert space in step 1, we first note that we can view the Slater determinants as nodes on a graph and view $\hat{H}$ as their connectivity matrix, where two determinants $\left|D_i\right\rangle$ and $\left|D_j\right\rangle$ are connected if and only if
\beq
H_{ij}=\left\langle D_i\left|\hat{H}\right|D_j\right\rangle\ne 0.
\eeq

Starting from a single determinant, which can be found using a mean-field theory such as Hartree-Fock, we iteratively explore the Hilbert space graph using a pruned breadth-first search. At each iteration of the search, the ground state $\left|\psi_0\right\rangle=\sum_{i\in\mathcal{V}} c_i \left|D_i\right\rangle$ within the current selected space $\mathcal{V}$ is obtained, and then new determinants in the next layer are added to $\mathcal{V}$ if they meet some criterion of importance.
In Heat-bath Configuration Interaction (HCI), which is an efficient instance of SCI+PT, the importance criterion is that a new determinant $\left|D_j\right\rangle$ is added if and only if
\beq
\max_{i\in \mathcal{V}} \left|H_{ji}c_i\right|>\epsilon_1,
\eeq
for some user-defined parameter $\epsilon_1$, which can be different on different iterations of the search.

The reason for the HCI selection criterion is as follows. In a large basis set, the majority of the excitations allowed by $\hat{H}$ correspond to the
term in $\hat{H}$ with the most creation and annihilation operators, $h^{n_{\rm max}}$. Before starting the HCI run, the components of this tensor can be computed and stored in the following way: For each set of annihilation operators, store a list of the possible corresponding sets of creation operators, as well as their corresponding Hamiltonian matrix element magnitudes, in decreasing order by these magnitudes. Then, on each step of the breadth-first search, for each determinant in the current selected space, iterate over all sets of occupied orbitals, and for each, traverse the corresponding sorted list until the cutoff is reached, i.e., until $\left|H_{ji}\right|<\epsilon_1/\left|c_i\right|$. This way, no time is wasted generating long lists of candidate exciations of the maximum excitation order, in contrast to other SCI+PT methods.

The breadth-first search proceeds until convergence or until memory limitations are reached, and the ground state within the selected space yields the variational wavefunction.

After computing the variational wavefunction, multireference Epstein-Nesbet perturbation theory~\cite{Eps-PR-26,Nes-PRS-55} is performed to improve the energy.
Due to the large number of negligible terms in the perturbative summation, a ``screened sum'' is introduced~\cite{Holmes2016},
\beq
E_2&=&\sum_k \frac{\left(\sum_j^{\left(\epsilon_2\right)} H_{kj} c_j\right)^2}{E_0-H_{kk}},
\eeq
where $\sum_j^{\left(\epsilon_2\right)} x_j$ denotes a sum over all terms $x_j$ such that $\left|x_j\right|>\epsilon_2$.
The reason for this form of the screened sum is that the same algorithm that was used to efficiently perform the breadth-first search can also be used to screen perturbative contributions, with (the much smaller) $\epsilon_2$ in the place of $\epsilon_1$.

In order to avoid storing the remaining list of non-negligible contributions to the sum, a \emph{semistochastic} algorithm~\cite{sharma2017semistochastic} is used, in which the largest contributions are summed deterministically, and the remaining, smaller contributions are summed stochastically. It should be noted that, in contrast to quantum Monte Carlo methods, the stochasticity here is merely used to evaluate a sum, and therefore there is no autocorrelation time or Fermion sign problem.

Finally, after the HCI total energy is computed as $E_{\rm HCI} = E_{0} + E_2$, extrapolation to the Full CI limit is performed by fitting $E_{\rm HCI}$ to a linear or quadratic function of $E_2$ and extrapolating to $E_2=0$.\cite{holmes2017excited}

While HCI can be applied to any second-quantized Hamiltonian, in this Letter we restrict ourselves to the quantum chemical Hamiltonian, which conserves particle number and only includes up to two-body interactions:
\beq
\hat{H} &=h_{\rm nuc.}+&\sum_{pr} f_{rp} a_{r}^{\dagger}a_{p}+ \frac{1}{2} \sum_{pqrs}g_{rspq}a_{r}^{\dagger}a_{s}^{\dagger}a_{q}a_{p}, % phys notation
\eeq
where $h_{\rm nuc.}$ is the nuclear-nuclear energy, and the indices $\{p,q,r,s\}$ incorporate both spatial and spin degrees of freedom.



\section{Chromium Dimer}
\label{related}

A prototypical example of a small but computationally very challenging molecule is the chromium dimer, whose ground-state potential energy curve is a great challenge to compute with state-of-the-art quantum chemistry methods, for several reasons.
It has a formal sextuple bond with a weak binding energy, short equilibrium length, and an unusual extended ``shoulder'' region as it is dissociated.
It is highly multiconfigurational, owing to its sextuple bond, with many configurations required to correctly dissociate the molecule into two high-spin atoms.
It exhibits differential electron correlation effects, scalar relativistic effects, and the importance of semi-core valence electron correlation~\cite{Muller2009}.
Due to its complex electronic structure, the chromium dimer has long been the quintessential proving ground for 
many-body algorithms in quantum chemistry~\cite{Dachsel1999,Angeli2001,Celani2004,Angeli2006,Muller2009,Mirzaei2010,Kurashige2011,Ruip2011,Seiffert2012,Purwanto2015,Guo2016}.

Both static and dynamic correlation are required for an accurate theoretical treatment. Algorithms that address only one of the two, such as Complete Active Space Self-Consistent Field (CASSCF) with 12 electrons in 12 orbitals, and coupled-cluster theory, which only addresses the dynamical correlation, fail spectacularly~\cite{Bauschlicher1994}.

To date, two of the most accurate algorithms for treating the chromium dimer are Auxiliary Field Quantum Monte Carlo (AFQMC)~\cite{Purwanto2015} and a Density Matrix Renormalization Group Self-Consistent reference (of 12 electrons in 22 orbitals) with N-electron Valence Perturbation Theory (DMRG-SC-NEVPT2)~\cite{Guo2016}.
%Even multireference calculations as large as a multireference perturbation theory on top of a (28e, 20o) DMRG-SCF reference are qualitatively wrong and can even give additional unphysical minima~\cite{Guo2016}.
However, both of these methods have serious drawbacks. In the case of AFQMC, the error bars are large, and the shoulder region is qualitatively too deep.
With DMRG-SC-NEVPT2, accurate results can only be obtained after extrapolating to the Complete Basis Set (CBS) limit and by carefully choosing the active space, since using a (28e, 20o) active space
rather than (12e, 22o) produces very poor results including a second unphysical minimum~\cite{Guo2016}.

In this Letter, we present an accurate binding curve using only the small cc-pVDZ-DK basis set, computed using our Heat-bath Configuration Interaction algorithm. Our calculations have the advantage of not requiring either extrapolation to the CBS limit or a choice of active space, beyond deciding which core orbitals to freeze.



\section{Results}
\label{results}
We performed calculations at various points along the Cr$_2$ potential energy surface using HCI.
Scalar relativistic effects were included using the second-order Douglas-Kroll-Hess Hamiltonian.
We used Dunning's basis sets~\cite{dunning1989gaussian} modified to be appropriate for Douglas-Kroll-Hess calculations, cc-pV$X$Z-DK.

We performed calculations with frozen argon cores (12 valence electrons) and frozen neon cores (28 valence electrons).
For each calculation, a CASSCF(12e, 12o) calculation was performed, and the resulting core orbitals were frozen (either Ar cores or Ne cores).
After freezing the cores, natural orbitals were obtained using a short HCI run, in order to accelerate convergence to the Full CI limit.
All electron integrals were computed using the PySCF quantum chemistry package~\cite{sun2017python}.

Extrapolation to the Full CI limit was performed by fitting a quadratic function of $\Delta E_2$ to the HCI total energy calculations and extrapolating to $\Delta E_2=0$.

\subsection{Frozen Ar core}
We performed frozen-core calculations using up to the cc-pVQZ-DK basis set (12e, 190o), and see that this yields a bound molecule, but with a binding curve that is qualitatively very far off from the experimental curve.
Despite Cr$_2$'s formal sextuple bond, six electron pairs
are not enough to qualitatively characterize the bond in our theoretical calculations, even with a large cc-pVQZ-DK basis set.
\begin{figure}
\includegraphics[width=0.5\textwidth]{energy_Cr2_2z_12e}
\end{figure}

\subsection{Frozen Ne core}
We then performed semi-core calculations using the cc-pVDZ-DK basis set (28e, 76o).
When semi-core excitations are included, the binding curve looks qualitatively correct, with a well of nearly the correct depth at nearly the correct geometry, and a ``shoulder'' structure, as found experimentally.
The remaining difference between theory and experiment could be due to a variety of factors, including basis set incompleteness,
higher-order relativistic effects beyond those treated perturbatively by the second-order Doughas-Kroll-Hess Hamiltonian, the interpretation of experimental data, and the correlation effects of the remaining core electrons.
\begin{figure}
\includegraphics[width=0.5\textwidth]{energy_Cr2_2z_28e}
\end{figure}

\section{Conclusion}
\label{conclusion}
We have presented an efficient quantum many-body algorithm, and demonstrated its effectiveness in solving for the ground-state potential energy surface of diatomic chromium, a prototypical example of a strongly-correlated system that is very difficult to accurately treat theoretically.
We have shown that, while fully correlating only the 12 valence electrons is not accurate enough, the inclusion of semi-core excitations (28 electrons) is sufficient to provide a qualitatively correct binding curve for this challenging molecule.

HCI thus has the potential to treat challenging systems with many strongly-correlated electrons efficiently and accurately. We are currently investing its use to study other strongly-correlated systems, such as the homogeneous electron gas and periodic systems.

\begin{acknowledgements}
\end{acknowledgements}

\bibliographystyle{jchemphys}
\bibliography{hci,cr2}

\end{document}
